package com.angel.mongodb.annotation;

/**
 * @description: 操作类型
 * @author: gankench@gmail.com
 * @create: 2021-04-07 19:44
 */
public enum OperateType {
    UNKNOWN("unknown"),
    SAVE("SAVE"),
    QUERY("QUERY"),
    COUNT("COUNT"),
    DELETE("DELETE"),
    UPDATE("update");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    OperateType(String type) {
        this.value = type;
    }
}
