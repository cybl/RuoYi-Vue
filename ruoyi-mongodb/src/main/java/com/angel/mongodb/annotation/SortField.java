package com.angel.mongodb.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description: sort表示排序类似，默认为asc正序
 * attribute表示要查询的属性，默认为空串，如果为空则为字段名称
 * @return:
 * @author: gankench@gmail.com
 * @time: 4/10/21 7:10 PM
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SortField {

    SortType sort() default SortType.ASC;

    String attribute() default "";
}
