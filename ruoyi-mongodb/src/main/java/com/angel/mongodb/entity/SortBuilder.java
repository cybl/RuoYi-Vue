package com.angel.mongodb.entity;

import com.angel.mongodb.utils.MongoUtils;
import com.ruoyi.common.utils.reflect.SerializableFunction;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import java.util.ArrayList;
import java.util.List;

public class SortBuilder {
    List<Order> orderList = new ArrayList<>();

    public SortBuilder() {
    }

    public SortBuilder(String column, Direction direction) {
        Order order = new Order(direction, column);
        orderList.add(order);
    }

    public SortBuilder(List<Order> orderList) {
        this.orderList.addAll(orderList);
    }

    public <T, R> SortBuilder(SerializableFunction<T, R> column, Direction direction) {
        Order order = new Order(direction, MongoUtils.getFieldName(column));
        orderList.add(order);
    }

    public SortBuilder add(String column, Direction direction) {
        Order order = new Order(direction, column);
        orderList.add(order);
        return this;
    }

    public <T, R> SortBuilder add(SerializableFunction<T, R> column, Direction direction) {
        Order order = new Order(direction, MongoUtils.getFieldName(column));
        orderList.add(order);
        return this;
    }

    public Sort toSort() {
        return Sort.by(orderList);
    }
}
