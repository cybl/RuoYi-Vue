package com.angel.mongodb.utils;

import com.ruoyi.common.utils.reflect.SerializableFunction;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Collection;

/**
 * 查询语句生成器 AND连接
 */
public class CriteriaAndWrapper<T> extends CriteriaWrapper {

    public CriteriaAndWrapper() {
        andLink = true;
    }

    public CriteriaAndWrapper and(Criteria criteria) {
        list.add(criteria);
        return this;
    }

    public CriteriaAndWrapper and(CriteriaWrapper criteriaWrapper) {
        list.add(criteriaWrapper.build());
        return this;
    }

    /**
     * 等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaWrapper
     */
    @Override
    public CriteriaAndWrapper eq(String column, Object params) {
        super.eq(column, params);
        return this;
    }

    /**
     * 等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaWrapper
     */
    public <T, R> CriteriaAndWrapper eq(SerializableFunction<T, R> column, Object params) {
        super.eq(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 不等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper ne(String column, Object params) {
        super.ne(column, params);
        return this;
    }

    /**
     * 不等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper ne(SerializableFunction<T, R> column, Object params) {
        super.ne(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 小于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper lt(String column, Object params) {
        super.lt(column, params);
        return this;
    }

    /**
     * 小于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper lt(SerializableFunction<T, R> column, Object params) {
        super.lt(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 小于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper lte(String column, Object params) {
        super.lte(column, params);
        return this;
    }

    /**
     * 小于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper lte(SerializableFunction<T, R> column, Object params) {
        super.lte(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 大于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper gt(String column, Object params) {
        super.gt(column, params);
        return this;
    }

    /**
     * 大于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper gt(SerializableFunction<T, R> column, Object params) {
        super.gt(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 大于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper gte(String column, Object params) {
        super.gte(column, params);
        return this;
    }

    /**
     * 大于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper gte(SerializableFunction<T, R> column, Object params) {
        super.gte(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 包含
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper contain(String column, Object params) {
        super.contain(column, params);
        return this;
    }

    /**
     * 包含
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper contain(SerializableFunction<T, R> column, Object params) {
        super.contain(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper containOr(String column, Collection<?> params) {
        super.containOr(column, params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper containOr(SerializableFunction<T, R> column, Collection<?> params) {
        super.containOr(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper containOr(String column, Object[] params) {
        super.containOr(column, params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper containOr(SerializableFunction<T, R> column, Object[] params) {
        super.containOr(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper containAnd(String column, Collection<?> params) {
        super.containAnd(column, params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper containAnd(SerializableFunction<T, R> column, Collection<?> params) {
        super.containAnd(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper containAnd(String column, Object[] params) {
        super.containAnd(column, params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper containAnd(SerializableFunction<T, R> column, Object[] params) {
        super.containAnd(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 相似于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper like(String column, String params) {
        super.like(column, params);
        return this;
    }

    /**
     * 相似于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper like(SerializableFunction<T, R> column, String params) {
        super.like(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper in(String column, Collection<?> params) {
        super.in(column, params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper in(SerializableFunction<T, R> column, Collection<?> params) {
        super.in(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper in(String column, Object[] params) {
        super.in(column, params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper in(SerializableFunction<T, R> column, Object[] params) {
        super.in(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper nin(String column, Collection<?> params) {
        super.nin(column, params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper nin(SerializableFunction<T, R> column, Collection<?> params) {
        super.nin(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper nin(String column, Object[] params) {
        super.nin(column, params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper nin(SerializableFunction<T, R> column, Object[] params) {
        super.nin(MongoUtils.getFieldName(column), params);
        return this;
    }

    /**
     * 为空
     *
     * @param column 字段
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper isNull(String column) {
        super.isNull(column);
        return this;
    }

    /**
     * 为空
     *
     * @param column 字段
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper isNull(SerializableFunction<T, R> column) {
        super.isNull(MongoUtils.getFieldName(column));
        return this;
    }

    /**
     * 不为空
     *
     * @param column 字段
     * @return CriteriaAndWrapper
     */
    @Override
    public CriteriaAndWrapper isNotNull(String column) {
        super.isNotNull(column);
        return this;
    }

    /**
     * 不为空
     *
     * @param column 字段
     * @return CriteriaAndWrapper
     */
    public <T, R> CriteriaAndWrapper isNotNull(SerializableFunction<T, R> column) {
        super.isNotNull(MongoUtils.getFieldName(column));
        return this;
    }

    /**
     * 数组查询
     *
     * @param arr    数组名
     * @param column 字段名
     * @param param  字段值
     * @return
     */
    @Override
    public CriteriaAndWrapper findArray(String arr, String column, String param) {
        super.findArray(arr, column, param);
        return this;
    }

    /**
     * 数组查询
     *
     * @param arr    数组名
     * @param column 字段名
     * @param param  字段值
     * @return
     */
    public <T, R> CriteriaAndWrapper findArray(SerializableFunction<T, R> arr, SerializableFunction<T, R> column, String param) {
        super.findArray(MongoUtils.getFieldName(arr), MongoUtils.getFieldName(column), param);
        return this;
    }

    /**
     * 数组模糊查询
     *
     * @param arr    数组名
     * @param column 字段名
     * @param param  字段值
     * @return
     */
    @Override
    public CriteriaAndWrapper findArrayLike(String arr, String column, String param) {
        super.findArrayLike(arr, column, param);
        return this;
    }

    /**
     * 数组模糊查询
     *
     * @param arr    数组名
     * @param column 字段名
     * @param param  字段值
     * @return
     */
    public <T, R> CriteriaAndWrapper findArrayLike(SerializableFunction<T, R> arr, SerializableFunction<T, R> column, String param) {
        super.findArrayLike(MongoUtils.getFieldName(arr), MongoUtils.getFieldName(column), param);
        return this;
    }
}
