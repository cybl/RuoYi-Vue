package com.angel.mongodb.core;

import com.angel.mongodb.annotation.MongoOperateLog;
import com.angel.mongodb.annotation.OperateType;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

/**
 * @description: MongoDB通用Dao
 * @author: gankench@gmail.com
 * @time: 4/9/21 6:57 PM
 */
public interface BaseMongoDAO {

//    /**
//     * 保存一个对象到mongodb
//     *
//     * @param object
//     * @return
//     */
//    public String saveOrUpdate(Object object);
//
//    /**
//     * 根据id删除对象
//     *
//     * @param t
//     */
//    public void deleteById(T t);
//
//    /**
//     * 根据对象的属性删除
//     *
//     * @param t
//     */
//    public void deleteByCondition(T t);
//
//
//    /**
//     * 根据id进行更新
//     *
//     * @param id
//     * @param t
//     */
//    public void updateById(String id, T t);
//
//
//    /**
//     * 根据对象的属性查询
//     *
//     * @param t
//     * @return
//     */
//    public List<T> findByCondition(T t);
//
//
//    /**
//     * 通过条件查询实体(集合)
//     *
//     * @param query
//     */
//    public List<T> find(Query query);
//
//    /**
//     * 通过一定的条件查询一个实体
//     *
//     * @param query
//     * @return
//     */
//    public T findOne(Query query);
//
//    /**
//     * 通过条件查询更新数据
//     *
//     * @param query
//     * @param update
//     * @return
//     */
//    public void update(Query query, Update update);
//
//    /**
//     * 通过ID获取记录
//     *
//     * @param id
//     * @return
//     */
//    public T findById(String id);
//
//    /**
//     * 通过ID获取记录,并且指定了集合名(表的意思)
//     *
//     * @param id
//     * @param collectionName 集合名
//     * @return
//     */
//    public T findById(String id, String collectionName);
//
//    /**
//     * 通过条件查询,查询分页结果
//     *
//     * @param page
//     * @param query
//     * @return
//     */
//    public Page<T> findPage(Page<T> page, Query query);
//
//    /**
//     * 求数据总和
//     *
//     * @param query
//     * @return
//     */
//    public long count(Query query);


    @MongoOperateLog(operateType = OperateType.SAVE)
    String saveOrUpdate(Object object);

    @MongoOperateLog(operateType = OperateType.SAVE)
    <T> void insertAll(List<T> list);

    @MongoOperateLog(operateType = OperateType.SAVE)
    void updateAllColumnById(Object object);

    @MongoOperateLog(operateType = OperateType.DELETE)
    Long deleteByQuery(Query query, Class<?> clazz);

    @MongoOperateLog(operateType = OperateType.UPDATE)
    Long updateFirst(Query query, Update update, Class<?> clazz);

    @MongoOperateLog(operateType = OperateType.UPDATE)
    Long updateMulti(Query query, Update update, Class<?> clazz);

    @MongoOperateLog(operateType = OperateType.QUERY)
    <T> T findById(String id, Class<T> clazz);

    @MongoOperateLog(operateType = OperateType.QUERY)
    <T> T findOneByQuery(Query query, Class<T> clazz);

    @MongoOperateLog(operateType = OperateType.QUERY)
    <T> List<T> findListByQuery(Query query, Class<T> clazz);

    @MongoOperateLog(operateType = OperateType.QUERY)
    List<String> findIdsByQuery(Query query, Class<?> clazz);

    @MongoOperateLog(operateType = OperateType.COUNT)
    Long findCountByQuery(Query query, Class<?> clazz);


    /**
     * 获取MongoDB模板操作
     *
     * @return
     */
    public MongoTemplate getMongoTemplate();

}
