package com.ruoyi.common.core.dao;

import com.angel.mongodb.core.MongoDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * @description: 基础dao
 * @author: gankench@gmail.com
 * @create: 2021-04-10 14:28
 */
@Repository
public class BaseDao extends MongoDaoSupport {

}
