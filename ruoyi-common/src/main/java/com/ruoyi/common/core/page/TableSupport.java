package com.ruoyi.common.core.page;

import com.angel.mongodb.entity.Page;
import com.ruoyi.common.utils.ServletUtils;

/**
 * 表格数据处理
 *
 * @author ruoyi
 */
public class TableSupport {
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 封装分页对象
     */
    public static Page getPage() {
        Page page = new Page();
        page.setPageNum(ServletUtils.getParameterToInt(PAGE_NUM));
        page.setPageSize(ServletUtils.getParameterToInt(PAGE_SIZE));
        page.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        page.setIsAsc(ServletUtils.getParameter(IS_ASC));
        return page;
    }

    public static Page buildPageRequest() {
        return getPage();
    }
}
