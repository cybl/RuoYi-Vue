package com.ruoyi.web.controller.monitor;

import cn.hutool.core.bean.BeanUtil;
import com.angel.mongodb.utils.Wraps;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysOperLog;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.core.service.impl.BaseService;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.viewObject.SysOperLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @description: 操作日志记录
 * @author: gankench@gmail.com
 * @time: 4/10/21 6:44 PM
 */
@RestController
@RequestMapping("/monitor/operlog")
public class SysOperlogController extends BaseController {
    @Autowired
    private BaseService operLogService;

    @PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysOperLogVo operLog) {
        SysOperLog sysOperLog = BeanUtil.copyProperties(operLog, SysOperLog.class);
        List<SysOperLogVo> list = operLogService.findPage(Wraps.buildBasePageQuery(sysOperLog, TableSupport.buildPageRequest()), SysOperLog.class)
                .stream()
                .map(item -> BeanUtil.copyProperties(item, SysOperLogVo.class))
                .collect(Collectors.toList());
        return getDataTable(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:export')")
    @GetMapping("/export")
    public AjaxResult export(SysOperLogVo operLog) {
        SysOperLog sysOperLog = BeanUtil.copyProperties(operLog, SysOperLog.class);
        List<SysOperLogVo> list = operLogService.findListByQuery(Wraps.buildBaseQuery(sysOperLog), SysOperLog.class)
                .stream()
                .map(item -> BeanUtil.copyProperties(item, SysOperLogVo.class))
                .collect(Collectors.toList());
        ExcelUtil<SysOperLogVo> util = new ExcelUtil<SysOperLogVo>(SysOperLogVo.class);
        return util.exportExcel(list, "操作日志");
    }

    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public AjaxResult remove(@PathVariable List<String> operIds) {
        return toAjax(operLogService.deleteByIds(operIds, SysOperLog.class).intValue());
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public AjaxResult clean() {
        operLogService.cleanData(SysOperLog.class);
        return AjaxResult.success();
    }
}
