package com.ruoyi.web.controller.monitor;

import cn.hutool.core.bean.BeanUtil;
import com.angel.mongodb.utils.Wraps;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysLoginInfo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.core.service.impl.BaseService;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.viewObject.SysLogininfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @description: 系统访问记录
 * @author: gankench@gmail.com
 * @time: 4/7/21 11:34 AM
 */
@RestController
@RequestMapping("/monitor/loginInfo")
public class SysLoginInfoController extends BaseController {
    @Autowired
    private BaseService loginInfoService;

    @PreAuthorize("@ss.hasPermi('monitor:loginInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysLogininfoVo loginInfo) {
        SysLoginInfo login = BeanUtil.copyProperties(loginInfo, SysLoginInfo.class);
        List<SysLogininfoVo> list = loginInfoService.findPage(Wraps.buildBasePageQuery(login, TableSupport.buildPageRequest()), SysLoginInfo.class)
                .stream()
                .map(item -> BeanUtil.copyProperties(item, SysLogininfoVo.class))
                .collect(Collectors.toList());
        return getDataTable(list);
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:loginInfo:export')")
    @GetMapping("/export")
    public AjaxResult export(SysLogininfoVo loginInfo) {
        SysLoginInfo login = BeanUtil.copyProperties(loginInfo, SysLoginInfo.class);
        List<SysLogininfoVo> list = loginInfoService.findListByQuery(Wraps.andWrapper(login), SysLoginInfo.class)
                .stream()
                .map(item -> BeanUtil.copyProperties(item, SysLogininfoVo.class))
                .collect(Collectors.toList());
        ExcelUtil<SysLogininfoVo> util = new ExcelUtil<SysLogininfoVo>(SysLogininfoVo.class);
        return util.exportExcel(list, "登录日志");
    }

    @PreAuthorize("@ss.hasPermi('monitor:loginInfo:remove')")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public AjaxResult remove(@PathVariable List<String> infoIds) {
        return toAjax(loginInfoService.deleteByIds(infoIds, SysLoginInfo.class).intValue());
    }

    @PreAuthorize("@ss.hasPermi('monitor:loginInfo:remove')")
    @Log(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public AjaxResult clean() {
        loginInfoService.cleanData(SysLoginInfo.class);
        return AjaxResult.success();
    }
}
