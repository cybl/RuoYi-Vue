package com.angel.flowable.vo;

import lombok.Data;

/**
 * @author kubilewang
 * @date 2020年3月24日
 */
@Data
public class IdentityResponse {
    private String identityId;
    private String identityType;
    private String identityName;
}
