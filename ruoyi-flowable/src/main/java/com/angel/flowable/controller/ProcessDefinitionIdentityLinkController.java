package com.angel.flowable.controller;

import com.angel.flowable.common.BaseFlowableController;
import com.angel.flowable.service.ProcessDefinitionService;
import com.angel.flowable.vo.IdentityRequest;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.identitylink.api.IdentityLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author kubilewang
 * @date 2020年3月24日
 */
@RestController
@RequestMapping("/flowable/processDefinitionIdentityLink")
public class ProcessDefinitionIdentityLinkController extends BaseFlowableController {
    @Autowired
    private ProcessDefinitionService processDefinitionService;

    @PreAuthorize("@ss.hasPermi('flowable:processDefinitionIdentityLink:list')")
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam String processDefinitionId) {
        ProcessDefinition processDefinition = processDefinitionService.getProcessDefinitionById(processDefinitionId);
        List<IdentityLink> identityLinks =
                repositoryService.getIdentityLinksForProcessDefinition(processDefinition.getId());
        return AjaxResult.success(responseFactory.createIdentityResponseList(identityLinks));
    }

    @Log(title = "新增流程定义授权")
    @PreAuthorize("@ss.hasPermi('flowable:processDefinitionIdentityLink:save')")
    @PostMapping(value = "/save")
    public AjaxResult save(@RequestBody IdentityRequest identityRequest) {
        processDefinitionService.saveProcessDefinitionIdentityLink(identityRequest);
        return AjaxResult.success();
    }

    @Log(title = "删除流程定义授权")
    @PreAuthorize("@ss.hasPermi('flowable:processDefinitionIdentityLink:delete')")
    @DeleteMapping(value = "/delete")
    public AjaxResult delete(@RequestParam String processDefinitionId, @RequestParam String identityId,
                             @RequestParam String identityType) {
        processDefinitionService.deleteProcessDefinitionIdentityLink(processDefinitionId, identityId, identityType);
        return AjaxResult.success();
    }
}
