package com.angel.flowable.controller;


import com.angel.flowable.common.BaseFlowableController;
import com.angel.flowable.service.FlowableTaskService;
import com.angel.flowable.vo.IdentityRequest;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import org.flowable.identitylink.api.history.HistoricIdentityLink;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author kubilewang
 * @date 2020年3月24日
 */
@RestController
@RequestMapping("/flowable/taskIdentityLink")
public class TaskIdentityLinkController extends BaseFlowableController {
    @Autowired
    protected FlowableTaskService flowableTaskService;

    @PreAuthorize("@ss.hasPermi('flowable:taskIdentityLink:list')")
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam String taskId) {
        HistoricTaskInstance task = flowableTaskService.getHistoricTaskInstanceNotNull(taskId);
        List<HistoricIdentityLink> historicIdentityLinks = historyService.getHistoricIdentityLinksForTask(task.getId());
        return AjaxResult.success(responseFactory.createTaskIdentityResponseList(historicIdentityLinks));
    }

    @Log(title = "新增任务授权")
    @PreAuthorize("@ss.hasPermi('flowable:taskIdentityLink:save')")
    @PostMapping(value = "/save")
    public AjaxResult save(@RequestBody IdentityRequest taskIdentityRequest) {
        flowableTaskService.saveTaskIdentityLink(taskIdentityRequest);
        return AjaxResult.success();
    }

    @Log(title = "删除任务授权")
    @PreAuthorize("@ss.hasPermi('flowable:taskIdentityLink:delete')")
    @DeleteMapping(value = "/delete")
    public AjaxResult deleteIdentityLink(@RequestParam String taskId, @RequestParam String identityId,
                                         @RequestParam String identityType) {
        flowableTaskService.deleteTaskIdentityLink(taskId, identityId, identityType);
        return AjaxResult.success();
    }
}
