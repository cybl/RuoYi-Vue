package com.angel.flowable.controller;

import com.angel.flowable.common.BaseFlowableController;
import com.angel.flowable.common.FlowablePage;
import com.angel.flowable.constant.FlowableConstant;
import com.angel.flowable.service.ProcessInstanceService;
import com.angel.flowable.vo.ProcessInstanceDetailResponse;
import com.angel.flowable.vo.ProcessInstanceRequest;
import com.angel.flowable.wapper.CommentListWrapper;
import com.angel.flowable.wapper.ProcInsListWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.CommonUtil;
import com.ruoyi.common.utils.ObjectUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.flowable.common.engine.api.query.QueryProperty;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.flowable.engine.impl.HistoricProcessInstanceQueryProperty;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kubilewang
 * @date 2020年3月23日
 */
@RestController
@RequestMapping("/flowable/processInstance")
public class ProcessInstanceController extends BaseFlowableController {

    private static Map<String, QueryProperty> allowedSortProperties = new HashMap<>();

    @Autowired
    private ProcessInstanceService processInstanceService;

    static {
        allowedSortProperties.put(FlowableConstant.ID, HistoricProcessInstanceQueryProperty.PROCESS_INSTANCE_ID_);
        allowedSortProperties.put(FlowableConstant.PROCESS_DEFINITION_ID,
                HistoricProcessInstanceQueryProperty.PROCESS_DEFINITION_ID);
        allowedSortProperties.put(FlowableConstant.PROCESS_DEFINITION_KEY,
                HistoricProcessInstanceQueryProperty.PROCESS_DEFINITION_KEY);
        allowedSortProperties.put(FlowableConstant.BUSINESS_KEY, HistoricProcessInstanceQueryProperty.BUSINESS_KEY);
        allowedSortProperties.put("startTime", HistoricProcessInstanceQueryProperty.START_TIME);
        allowedSortProperties.put("endTime", HistoricProcessInstanceQueryProperty.END_TIME);
        allowedSortProperties.put("duration", HistoricProcessInstanceQueryProperty.DURATION);
        allowedSortProperties.put(FlowableConstant.TENANT_ID, HistoricProcessInstanceQueryProperty.TENANT_ID);
    }

    @PreAuthorize("@ss.hasPermi('flowable:processInstance:list')")
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam Map<String, String> requestParams) {
        HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();

        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID))) {
            query.processInstanceId(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.PROCESS_INSTANCE_NAME))) {
            query.processInstanceNameLike(requestParams.get(FlowableConstant.PROCESS_INSTANCE_NAME));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME))) {
            query.processDefinitionName(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY))) {
            query.processDefinitionKey(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID))) {
            query.processDefinitionId(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.BUSINESS_KEY))) {
            query.processInstanceBusinessKey(requestParams.get(FlowableConstant.BUSINESS_KEY));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.INVOLVED_USER))) {
            query.involvedUser(requestParams.get(FlowableConstant.INVOLVED_USER));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.FINISHED))) {
            boolean isFinished = ObjectUtils.convertToBoolean(requestParams.get(FlowableConstant.FINISHED));
            if (isFinished) {
                query.finished();
            } else {
                query.unfinished();
            }
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.SUPER_PROCESS_INSTANCE_ID))) {
            query.superProcessInstanceId(requestParams.get(FlowableConstant.SUPER_PROCESS_INSTANCE_ID));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.EXCLUDE_SUBPROCESSES))) {
            query.excludeSubprocesses(ObjectUtils.convertToBoolean(requestParams.get(FlowableConstant.EXCLUDE_SUBPROCESSES)));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.FINISHED_AFTER))) {
            query.finishedAfter(ObjectUtils.convertToDatetime(requestParams.get(FlowableConstant.FINISHED_AFTER)));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.FINISHED_BEFORE))) {
            query.finishedBefore(ObjectUtils.convertToDatetime(requestParams.get(FlowableConstant.FINISHED_BEFORE)));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.STARTED_AFTER))) {
            query.startedAfter(ObjectUtils.convertToDatetime(requestParams.get(FlowableConstant.STARTED_AFTER)));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.STARTED_BEFORE))) {
            query.startedBefore(ObjectUtils.convertToDatetime(requestParams.get(FlowableConstant.STARTED_BEFORE)));
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.STARTED_BY))) {
            query.startedBy(requestParams.get(FlowableConstant.STARTED_BY));
        }
        // startByMe 覆盖 startedBy
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.START_BY_ME))) {
            boolean isStartByMe = ObjectUtils.convertToBoolean(requestParams.get(FlowableConstant.START_BY_ME));
            if (isStartByMe) {
                query.startedBy(SecurityUtils.getLoginUser().getUser().getId());
            }
        }
        if (CommonUtil.isNotEmptyAfterTrim(requestParams.get(FlowableConstant.TENANT_ID))) {
            query.processInstanceTenantIdLike(requestParams.get(FlowableConstant.TENANT_ID));
        }

        FlowablePage page = this.pageList(requestParams, query, ProcInsListWrapper.class, allowedSortProperties,
                HistoricProcessInstanceQueryProperty.START_TIME);
        return AjaxResult.success(page);
    }

    @GetMapping(value = "/listMyInvolved")
    public AjaxResult listMyInvolved(@RequestParam Map<String, String> requestParams) {
        requestParams.put(FlowableConstant.INVOLVED_USER, SecurityUtils.getLoginUser().getUser().getId());
        return list(requestParams);
    }

    @GetMapping(value = "/queryById")
    public AjaxResult queryById(@RequestParam String processInstanceId) {
        permissionService.validateReadPermissionOnProcessInstance(SecurityUtils.getLoginUser().getUser().getId(), processInstanceId);
        ProcessInstance processInstance = null;
        HistoricProcessInstance historicProcessInstance =
                processInstanceService.getHistoricProcessInstanceById(processInstanceId);
        if (historicProcessInstance.getEndTime() == null) {
            processInstance = processInstanceService.getProcessInstanceById(processInstanceId);
        }
        ProcessInstanceDetailResponse pidr =
                responseFactory.createProcessInstanceDetailResponse(historicProcessInstance, processInstance);
        return AjaxResult.success(pidr);
    }

    @Log(title = "启动流程实例")
    @PostMapping(value = "/start")
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult start(@RequestBody ProcessInstanceRequest processInstanceRequest) {
        processInstanceService.start(processInstanceRequest);
        return AjaxResult.success();
    }

    @Log(title = "删除流程实例")
    @PreAuthorize("@ss.hasPermi('flowable:processInstance:delete')")
    @DeleteMapping(value = "/delete")
    public AjaxResult delete(@RequestParam String processInstanceId, @RequestParam(required = false) boolean cascade,
                             @RequestParam(required = false) String deleteReason) {
        processInstanceService.delete(processInstanceId, cascade, deleteReason);
        return AjaxResult.success();
    }

    @Log(title = "挂起流程实例")
    @PreAuthorize("@ss.hasPermi('flowable:processInstance:suspendOrActivate')")
    @PutMapping(value = "/suspend")
    public AjaxResult suspend(@RequestBody ProcessInstanceRequest processInstanceRequest) {
        processInstanceService.suspend(processInstanceRequest.getProcessInstanceId());
        return AjaxResult.success();
    }

    @Log(title = "激活流程实例")
    @PreAuthorize("@ss.hasPermi('flowable:processInstance:suspendOrActivate')")
    @PutMapping(value = "/activate")
    public AjaxResult activate(@RequestBody ProcessInstanceRequest processInstanceRequest) {
        processInstanceService.activate(processInstanceRequest.getProcessInstanceId());
        return AjaxResult.success();
    }

    @GetMapping(value = "/comments")
    public AjaxResult comments(@RequestParam String processInstanceId) {
        permissionService.validateReadPermissionOnProcessInstance(SecurityUtils.getLoginUser().getUser().getId(), processInstanceId);
        List<Comment> datas = taskService.getProcessInstanceComments(processInstanceId);
        Collections.reverse(datas);
        return AjaxResult.success(this.listWrapper(CommentListWrapper.class, datas));
    }

    @GetMapping(value = "/formData")
    public AjaxResult formData(@RequestParam String processInstanceId) {
        HistoricProcessInstance processInstance =
                permissionService.validateReadPermissionOnProcessInstance(SecurityUtils.getLoginUser().getUser().getId(), processInstanceId);
        Object renderedStartForm = formService.getRenderedStartForm(processInstance.getProcessDefinitionId());
        Map<String, Object> variables = null;
        if (processInstance.getEndTime() == null) {
            variables = runtimeService.getVariables(processInstanceId);
        } else {
            List<HistoricVariableInstance> hisVals =
                    historyService.createHistoricVariableInstanceQuery().processInstanceId(processInstanceId).list();
            variables = new HashMap<>(16);
            for (HistoricVariableInstance variableInstance : hisVals) {
                variables.put(variableInstance.getVariableName(), variableInstance.getValue());
            }
        }
        Map<String, Object> ret = new HashMap<String, Object>(4);
        boolean showBusinessKey = isShowBusinessKey(processInstance.getProcessDefinitionId());
        ret.put("showBusinessKey", showBusinessKey);
        ret.put(FlowableConstant.BUSINESS_KEY, processInstance.getBusinessKey());
        ret.put("renderedStartForm", renderedStartForm);
        ret.put("variables", variables);
        return AjaxResult.success(ret);
    }
}
