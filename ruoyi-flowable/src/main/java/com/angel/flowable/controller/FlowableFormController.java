package com.angel.flowable.controller;


import com.angel.flowable.domain.FlowableForm;
import com.angel.flowable.service.impl.FlowableFormServiceImpl;
import com.angel.mongodb.entity.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 流程Controller
 */
@RestController
@RequestMapping("/flowable/form")
public class FlowableFormController extends BaseController {
    @Autowired
    private FlowableFormServiceImpl flowableFormService;

    /**
     * 自定义查询列表
     *
     * @param flowableForm
     * @param current
     * @param size
     * @return
     */
    @PreAuthorize("@ss.hasPermi('flowable:form:list')")
    @GetMapping(value = "/list")
    public AjaxResult list(FlowableForm flowableForm, @RequestParam Integer current, @RequestParam Integer size) {
        Page<FlowableForm> page = new Page<>();
        page.setPageNum(current);
        page.setPageSize(size);
        List<FlowableForm> pageList = flowableFormService.list(page, flowableForm);
        return AjaxResult.success(pageList);
    }

    @PreAuthorize("@ss.hasPermi('flowable:form:list')")
    @GetMapping(value = "/queryById")
    public AjaxResult queryById(@RequestParam String id) {
        FlowableForm flowableForm = flowableFormService.findById(id, FlowableForm.class);
        return AjaxResult.success(flowableForm);
    }

    /**
     * @param flowableForm
     * @return
     * @功能：新增
     */
    @Log(title = "新增流程表单")
    @PreAuthorize("@ss.hasPermi('flowable:form:save')")
    @PostMapping(value = "/save")
    public AjaxResult save(@Valid @RequestBody FlowableForm flowableForm) {
        flowableFormService.save(flowableForm);
        return AjaxResult.success();
    }

    /**
     * @param flowableForm
     * @return
     * @功能：修改
     */
    @Log(title = "修改流程表单")
    @PreAuthorize("@ss.hasPermi('flowable:form:update')")
    @PutMapping(value = "/update")
    public AjaxResult update(@Valid @RequestBody FlowableForm flowableForm) {
        flowableFormService.updateById(flowableForm);
        return AjaxResult.success();
    }

    /**
     * @param ids
     * @return
     * @功能：批量删除
     */
    @Log(title = "删除流程表单")
    @PreAuthorize("@ss.hasPermi('flowable:form:delete')")
    @DeleteMapping(value = "/delete")
    public AjaxResult delete(@RequestParam String ids) {
        if (ids == null || ids.trim().length() == 0) {
            return AjaxResult.error("ids can't be empty");
        }
        String[] idsArr = ids.split(",");
        if (idsArr.length > 1) {
            flowableFormService.deleteByIds(Arrays.asList(idsArr), FlowableForm.class);
        } else {
            flowableFormService.deleteById(idsArr[0], FlowableForm.class);
        }
        return AjaxResult.success();
    }
}
