package com.angel.flowable.domain;


import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;


/**
 * @description:
 * @author: gankench@gmail.com
 * @time: 4/15/21 4:36 PM
 */
@Data
@Document
public class FlowableForm extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @MongoId
    private String formKey;


    private String formName;

    private String formJson;
}