package com.angel.flowable.config;

import org.flowable.image.ProcessDiagramGenerator;
import org.flowable.mongodb.cfg.MongoDbProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kubilewang
 * @date 2020年3月24日
 */
@Configuration
public class FlowableEngineConfig implements EngineConfigurationConfigurer<MongoDbProcessEngineConfiguration> {
    @Value("${kubilewang.flowable.font-name}")
    private String flowableFontName;

    @Override
    public void configure(MongoDbProcessEngineConfiguration engineConfiguration) {
        engineConfiguration.setConnectionUrl("mongodb://doudou-admin:admin.1qazXSW.password@angledoudou.top:27017,angledoudou.top:27018,angledoudou.top:27019/admin")
                .setDatabaseName("rouyi-vue")
                .setDisableIdmEngine(true);
        System.out.println("hello flowable");
        engineConfiguration.setProcessDiagramGenerator(processDiagramGenerator());
        engineConfiguration.setActivityFontName(flowableFontName);
        engineConfiguration.setLabelFontName(flowableFontName);
        engineConfiguration.setAnnotationFontName(flowableFontName);
    }

    @Bean
    public MongoDbProcessEngineConfiguration engineConfiguration() {
//        ProcessEngine processEngine = new MongoDbProcessEngineConfiguration()
//                .setConnectionUrl("mongodb://doudou-admin:admin.1qazXSW.password@angledoudou.top:27017,angledoudou.top:27018,angledoudou.top:27019/admin")
//                .setDatabaseName("rouyi-vue")
//                .setDisableIdmEngine(true)
//                .buildProcessEngine();

//        return new MongoDbProcessEngineConfiguration()
//                .setConnectionUrl("mongodb://doudou-admin:admin.1qazXSW.password@angledoudou.top:27017,angledoudou.top:27018,angledoudou.top:27019/admin")
//                .setDatabaseName("rouyi-vue")
//                .setDisableIdmEngine(true);
        return new MongoDbProcessEngineConfiguration();
    }

    @Bean
    public ProcessDiagramGenerator processDiagramGenerator() {
        return new CustomProcessDiagramGenerator();
    }
}
