package com.angel.flowable.service;


import com.angel.flowable.domain.FlowableForm;
import com.angel.mongodb.entity.Page;

import java.util.List;

/**
 * 流程表单Service
 *
 * @author kubilewang
 */
public interface FlowableFormService {
    /**
     * 分页查询流程表单
     *
     * @param page
     * @param flowableForm
     * @return
     */
    List<FlowableForm> list(Page page, FlowableForm flowableForm);
}
