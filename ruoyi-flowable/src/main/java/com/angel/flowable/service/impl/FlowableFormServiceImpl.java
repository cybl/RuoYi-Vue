package com.angel.flowable.service.impl;


import com.angel.flowable.domain.FlowableForm;
import com.angel.flowable.service.FlowableFormService;
import com.angel.mongodb.entity.Page;
import com.angel.mongodb.utils.Wraps;
import com.ruoyi.common.core.service.impl.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流程Service
 *
 * @author kubilewang
 */
@Service
public class FlowableFormServiceImpl extends BaseService implements FlowableFormService {

    @Override
    public List<FlowableForm> list(Page page, FlowableForm flowableForm) {
        return super.findListByQuery(Wraps.buildBasePageQuery(flowableForm, page), FlowableForm.class);
    }
}
